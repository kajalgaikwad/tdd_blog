require 'rails_helper'

RSpec.feature 'Posts list' do 
  scenario 'unauthenticated post' do 
  	visit posts_path
  	 within '#content' do
  	 	expect(find('h1')).to have_content('Blog Posts')
  	 end
  	end
end