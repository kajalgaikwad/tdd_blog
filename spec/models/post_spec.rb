require 'rails_helper'

RSpec.describe Post, type: :model do
 context 'validation tests' do
 	it 'ensures title presence' do
 		post = Post.new(body: 'body of title').save
 		expect(post).to eq(false)
 	end

 	it 'ensures body presence' do
 		post = Post.new(title: 'title').save
 		expect(post).to eq(false)
 	end

 	it 'should save successfully' do
 	    post = Post.new(title:'title',body: 'body of title').save
 		expect(post).to eq(true)
    end

 end

end
